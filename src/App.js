import React, { Component } from 'react';
import {Route,Switch,withRouter} from 'react-router-dom';
import Categories from "./component/Categories/Categories";
import Locations from "./component/Locations/Locations";
import GoogleMap from './component/googleMap/googleMap';

import './App.css';

class App extends Component {

/**/

  render() {
    return (
        <div>
           <Switch>
            <Route path ="/" exact component={Categories} />
            <Route path ="/locations" exact component={Locations} />
            <Route path ="/map" exact component ={GoogleMap} />
           </Switch>
           </div>
    );
  }
}


export default withRouter(App);
