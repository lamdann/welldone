/**
 * Created by Lamdan on 6/8/2018.
 */
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import "./googleMap.css"


const AnyReactComponent = ({ text }) =>
    <div className="markerDiv">
       <div>{text} </div>
    <div className="markerImage"> </div>
    </div>;

class SimpleMap extends Component {
    static defaultProps = {
        center: {
            lat: 59.95,
            lng: 30.33
        },
        zoom: 13
    };

    constructor(props){
        super(props);
        this.center = this.props.location.center?this.props.location.center:this.props.center;
        this.name = this.props.location.name?this.props.location.name:"default"
    }

    render() {
        return (
            <div style={{ height: '100vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyD12WrQqw8uUyOh82NzUIYa4vS0DB6UhpM" }}
                    defaultCenter={this.center}
                    defaultZoom={this.props.zoom} >

                    <AnyReactComponent
                        lat={this.center.lat}
                        lng={this.center.lng}
                        text={this.name}
                    />

                </GoogleMapReact>
            </div>
        );
    }
}
export default SimpleMap;
