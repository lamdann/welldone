/**
 * Created by Lamdan on 6/6/2018.
 */
import React, {Component} from "react";
import './Footer.css';
//import PropTypes from 'prop-types';
import {withRouter}from 'react-router-dom';
/*import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';*/

class Footer extends Component{

   constructor(props){
       super(props);
       this.state = { selected:[{Categories:false},{Locations:false}]};
       this.state.selected[props.selectedButton]=true;
   }

   isSelected=(name)=> {
       return this.state.selected[name];
   };
    categoriesHandler = ()=>{
      if(!this.isSelected("Categories")) {
          console.log("Categories selected");
          return this.props.history.push({pathname:"/",selectedButton:"Categories"});
      }

    };
    locationsHandler =()=>{
       if(!this.isSelected("Locations")) {
           return this.props.history.push({pathname:"/Locations",selectedButton:"Locations"});
          }
    };

   render(){
       return(
               <div className="footerBar">
                   <div>
                       <button className={this.isSelected("Categories")?"btn-primary active":"btn-primary"}
                               onClick={()=>this.categoriesHandler()}
                       >Categories <i className="glyphicon glyphicon-list"/></button>
                   </div>
                <div>
                    <button className={this.isSelected("Locations")?"btn-primary active":"btn-primary"}
                            onClick={()=>this.locationsHandler()}
                    >Locations <i className="glyphicon glyphicon-picture "/></button>
                </div>

               </div>

       );
   }

}
Footer.defaultProps={
    selectedButton:"Categories"
};


export default (withRouter(Footer));
