/**
 * Created by Lamdan on 6/6/2018.
 */
import React, {Component} from "react";
import "./Categories.css";
import Footer from "../foooter/Footer";
import { connect } from 'react-redux';
import {withRouter}from 'react-router-dom';
import * as actionCreators from '../../store/actions/index';


class Categories extends Component{

    constructor(props){
        super(props);
        this.state ={AddCategory:false,newCategoryName:"",editCategory:false,editValue:""};
        this.inputRef = React.createRef();
        this.inputEditRef = React.createRef();

    }

    canselNewCategory =()=>{
      this.setState({"AddCategory":false,newCategoryName:""});
    };
    addNewCategory=()=>{
        this.setState({"AddCategory":true});
        this.inputRef.current.focus();


    };
    onInputAddCategoryName =(e)=>{
      this.setState({newCategoryName:e.target.value})
    };
    onInputEditCategoryName =(e)=>{
        this.setState({editValue:e.target.value})
    };
    onEditSaveCategoryName =(selected)=>{
      if(this.state.editValue.length>0 && !this.checkIdentity(this.state.editValue)) {
          this.props.onEdit({name:selected,newName:this.state.editValue});
          this.setState({editValue:'',editCategory:false});
      }
      else{
          alert(this.state.editValue.length>0?this.state.editValue+" Already Contained":"No Empty Input")
      }
    };
    checkIdentity=(name)=>{
        return this.props.cat.Categories.find((item)=>{
          return item.name===name; });

    };
    onSaveCategoryName =()=>{
     if(this.state.newCategoryName.length>0&&!this.checkIdentity(this.state.newCategoryName)){
         this.props.onAdd({name:this.state.newCategoryName});
         this.setState({"AddCategory":false,newCategoryName:""});
     }
     else{
         alert(this.state.newCategoryName.length>0?this.state.newCategoryName+" Already added":"No Empty Input")
     }
    };

    getSelected =()=>{
        let selected =[];
        this.props.cat.Categories.map((item)=>{
            if(item.selected){ selected.push({...item}) }
            return item;
        });
        return selected;
    };

    onDeleteCategory =()=>{
      let selected =this.getSelected();
      if(selected.length>0){
          this.props.onDelete(selected)
      }else{alert(" Select What to delete") }
    };

    onEditButtonHandler=()=>{
      let selected = this.getSelected();
      if(selected.length===1){
          this.setState({editCategory:true})
      }else if (selected.length>1){
        alert("Please Edit Category one by one.\n Select only one Category");
      }else if(selected.length<1){
          alert("Choose what to edit ore create one")
      }


    };

    onCanselEditHandler =()=>{
      this.setState({editCategory:false,editValue:""})
    };

    getAddNewCategory = ()=>{
        return(
            <div className={this.state.AddCategory?"AddCategory modalOpen":"AddCategory modalClose"}>
                <div className= "AddCategoryForm">
                    <div className="form-group-sm AddCategoryFormInputBox">
                        <div>
                            <input type="text" id ="categoryName"
                                   placeholder="Type new Category Name"
                                   value={this.state.newCategoryName}
                                   onChange={this.onInputAddCategoryName}
                                   ref ={this.inputRef}
                                   className="form-control AddCategoryFormInput" />
                        </div>
                        <button className="btn-primary" onClick={this.onSaveCategoryName}>save</button>
                        <button className="btn-primary" onClick={this.canselNewCategory}>cansel</button>
                    </div>
                </div>
            </div>
        )
    };


getEditCategory =()=>{
    let selected = this.getSelected();
    if(selected.length<1) return null;
    return(
        <div className={this.state.editCategory?"AddCategory modalOpen":"AddCategory modalClose"}>
            <div className= "AddCategoryForm">
                <div className="form-group-sm AddCategoryFormInputBox">
                    <div>
                        <input type="text" id ="categoryName"
                               placeholder={selected[0].name}
                               onFocus={(e)=>{e.target.value =selected[0].name;this.setState({editValue:selected[0].name})}}
                               value={this.state.editValue}
                               onChange={this.onInputEditCategoryName}
                               ref ={this.inputEditRef}
                               className="form-control AddCategoryFormInput" />
                    </div>
                    <button className="btn-primary" onClick={()=>this.onEditSaveCategoryName(selected[0].name)}>save</button>
                    <button className="btn-primary" onClick={this.onCanselEditHandler}>cansel</button>
                </div>
            </div>
        </div>
    )

};











    render(){
      let categories = this.props.cat.Categories.map((item)=>{
          return(
              <li key={item.name} className={item.selected?"selected":" "}
              onClick={()=>this.props.onSelect({...item})}>
              {item.name}</li>
          )
        });
        return(
            <div>
                {this.getAddNewCategory()}
                {this.getEditCategory()}

            <div>
                <nav className="navbar navbar-expand-sm headerBar container">
                    <a className="navbar-brand" style={{fontSize:"22px"}}>My Category</a>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <button className=" btn-primary"
                             onClick={this.addNewCategory}
                            >AddCategory</button>
                        </li>
                        <li className="nav-item">
                            <button className={this.props.cat.Categories.length>0?"btn-primary":"btn"}
                                    disabled={!this.props.cat.Categories.length>0}
                                    onClick={this.onDeleteCategory}
                            >Delete</button>
                        </li>
                        <li className="nav-item">
                            <button className={this.props.cat.Categories.length>0?"btn-primary":"btn"}
                                    disabled ={!this.props.cat.Categories.length>0}
                                    onClick={this.onEditButtonHandler}
                            >Edit</button>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className="Categories">
                <div>
                    <ul className="categoryList">{categories}</ul>
                </div>
                <Footer selectedButton ="Categories"/>
            </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        cat: state
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onDelete:(value) => dispatch(actionCreators.delete_c(value)),
        onEdit: (value) => dispatch(actionCreators.edit_c(value)),
        onAdd:(value)=>dispatch(actionCreators.add_c(value)),
        onSelect:(value)=>dispatch(actionCreators.select_c(value))

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Categories));



