/**
 * Created by Lamdan on 6/6/2018.
 */
import React, {Component} from "react";
import "./Locations.css";
import Footer from "../foooter/Footer";
import {connect} from 'react-redux';
import {withRouter}from 'react-router-dom';
import * as actionCreators from '../../store/actions/index';
import TAG from "../../myGat";


//React.createRef();
class Locations extends Component {
    constructor(props) {
        super(props);
        console.log(TAG + "_PROPS_", props);
        this.state = {NamePicked: "", AddLocation: false, AddLocationFormValid: false,
            EditObject:null,EditFormValid:false,EditLocation:false};
        this.AddLocation = {name: "", Address: "", Coordinates: [{lat: ""}, {lon: ""}], Category: "", selected: false};
        this.AddLocationRef = [];
        this.selected={};
    }

   //=====================================Validation=====================================================================


 //---------------------------------------------------------------------------------------------------------------
    isFormValid =(obj)=>{
        return  obj && obj.name.length >0 && obj.Address.length >0 &&
                obj.Category.length >0 && obj.Coordinates[0].lat.length >0 &&
                obj.Coordinates[1].lon.length >0 ;
    };

//----------------------------------------------------------------------------------------------------------------
    invalidateInput = (el: Node) => {
        el.previousSibling.setAttribute('class', "glyphicon glyphicon-hand-left form-control-feedback has-error ");
        el.previousSibling.setAttribute("style", "color: red;");
    };
 //----------------------------------------------------------------------------------------------------------------------
    isInputValid = (e) => {
        if (e.target.value.length > 0) {
            e.target.previousSibling.setAttribute('class', "glyphicon glyphicon-ok form-control-feedback has-success");
            e.target.previousSibling.setAttribute("style", "color: green;");
        }
        else {
            e.target.previousSibling.setAttribute('class', "glyphicon glyphicon-hand-left form-control-feedback has-error ");
            e.target.previousSibling.setAttribute("style", "color: red;");
            this.setState({AddLocationFormValid: false,EditFormValid:false});
        }
    };


  //==================================================================================================================

  //================================Delete locations=============================================================================

    onDeleteLocationHandler = () => {
        let selected = this.getSelectedLocations();
        if (selected.length > 0) {
            let itemToDelete = [];
            selected.forEach((sItem) => {
                itemToDelete.push({categoryName: sItem.Category, jsonValue: JSON.stringify(sItem)})
            });
            this.props.onDelete(itemToDelete);
        } else {
            alert("Choose What to Delete or Create One");
        }
    };

   //==================================================================================================================

  //===================================Show Locations to User============================================================
    getSelectedCategories = () => {
        let selected = [];
        this.props.loc.Categories.map((item) => {
            if (item.selected) {
                selected.push({...item})
            }
            return item;
        });
        return selected;
    };

    getSelectedLocations = () => {
        return this.props.loc.myLocations.filter((item) => {
            return item.selected;
        });
    };



    getLocationForName = (name) => {
        let locations = this.props.loc.myLocations.filter((item) => {
            return item.Category === name;
        });
        return locations.map((item) => {
            let k = item["Coordinates"][0].lat + " " + item["Coordinates"][1].lon;

            return (

                    <div key={k} className={item.selected ? "LocationBox Selected" : "LocationBox"}
                         onClick={() => this.props.onSelect({Category: item.Category, item: JSON.stringify(item)})}>
                        <p>{"Name: " + item["name"]}</p>
                        <p>{"Address: " + item["Address"]}</p>
                        <p>{`Coordinates : lan  ${item["Coordinates"][0].lat}  lon: ${item["Coordinates"][1].lon}`} </p>
                        <p>{"Category: " + item["Category"]}</p>
                    </div>
                );
        })
    };

    getLocationsOfSelectedCat = () => {
        let selected = this.getSelectedCategories();
        selected = selected.filter((item) => {
            return this.props.loc.myLocations.find((loc) => {
                return loc.Category === item.name;
            });
        });
        let items = selected.map((sel, index) => {
            return (
                   <div key={sel.name} className="chestElement" >
                       <p key={index}>{sel.name}</p>
                       <div key={ sel.name + " " + {index} }>
                           {this.getLocationForName(sel.name)}
                       </div>
                   </div>

            )
        });
        return (items );
    };

    getCategoryNameAsOption = () => {
        return (
            this.props.loc.Categories.map((item) => {
                return (
                    <option key={item.name} value={item.name}>{item.name}</option>
                )
            })
        )
    };


  //===========================Edit Location ==========================================================================
    getSelected =()=>{
       return this.props.loc.myLocations.filter((el)=>{
           let categ = this.props.loc.Categories.find((categ)=>categ.name === el.Category);
           return categ&&categ.selected&&el.selected;
       });
    };

    onEditHandler = ()=>{
        this.selected =this.getSelected();
        if(this.selected.length<1){alert("Choose what to Edit")}
        else if(this.selected&& this.selected.length===1){

                this.setState({EditObject:this.selected[0]});
                setTimeout(()=>{this.setState({EditLocation:true})},200);

        }else{
            alert("You can't Edit more then one node at the time!")
        }
    };
    onEditSelectHandler =(e)=>{
       let editSelected = {...this.state.EditObject};
           editSelected.Category = e.target.value;
           this.setState({"EditObject":editSelected});
        if( this.isFormValid(this.state.EditObject)){
            this.setState({EditFormValid:true})
        }
    };
    onEditLocationName=(e)=>{
        let editSelected = {...this.state.EditObject};
        editSelected.name = e.target.value;
        this.setState({"EditObject":editSelected});
        if( this.isFormValid(this.state.EditObject)){
            this.setState({EditFormValid:true})
        }
    };
    onEditLocationAddress =(e)=>{
        let editSelected = {...this.state.EditObject};
        editSelected.Address = e.target.value;
        this.setState({"EditObject":editSelected});
        if( this.isFormValid(this.state.EditObject)){
            this.setState({EditFormValid:true})
        }
    };
    onEditLocationLat=(e)=>{
    let editSelected = {...this.state.EditObject};
    editSelected.Coordinates[0].lat = e.target.value;
    this.setState({"EditObject":editSelected});
        if( this.isFormValid(this.state.EditObject)){
            this.setState({EditFormValid:true})
        }
    };
    onEditLocationLon=(e)=>{
        let editSelected = {...this.state.EditObject};
        editSelected.Coordinates[1].lon = e.target.value;
        this.setState({"EditObject":editSelected});
       if( this.isFormValid(this.state.EditObject)){
           this.setState({EditFormValid:true})
       }
    };
    onSaveEditLocation =()=>{
      let Coordinates =[...this.state.EditObject.Coordinates];
      let editedObject = {...this.state.EditObject};

      editedObject["Coordinates"]=Coordinates;
      this.props.onEdit({"selected":{...this.selected[0]},editedObject});
       setTimeout(()=>{this.setState({EditObject:null})},1200);
        this.setState({EditFormValid:false,EditLocation:false});
        this.selected ={};


    };
    canselEditLocation =()=>{
    this.setState({EditFormValid:false,EditLocation:false});
       setTimeout(()=>{this.setState({EditObject:null})},1200);

    };

    //------------------------------------------------------------------------------------------------------------
    getEditLocation = () => {
       if(!this.state.EditObject)return null;
        return (<div className={this.state.EditLocation ? "AddLocation modalOpen" : "AddLocation modalClose"}>
            <div className="AddLocationForm">
                <div className="form-group-sm AddLocationFormInputBox">
                    <div className="row">
                        <div className="col-md-6 ">
                            <select className="form-control"
                                    onChange={this.onEditSelectHandler}
                                    value={this.state.EditObject.Category} >
                                <option default="ChooseCategoryName" value=''>ChooseCategoryName</option>
                                {this.getCategoryNameAsOption()}
                            </select>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <span> </span>
                                <input type="text" id="LocationName"
                                       placeholder={this.state.EditObject.name}
                                       onFocus={(e)=>{e.target.value = this.state.EditObject.name; }}
                                       onChange={(e) => {
                                           this.onEditLocationName(e);
                                           this.isInputValid(e)
                                       }}
                                       className="form-control AddCategoryFormInput"/>
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <span> </span>
                                <input type="Address" className="form-control"
                                       placeholder={this.state.EditObject.Address}
                                       onChange={(e) => {
                                           this.onEditLocationAddress(e);
                                           this.isInputValid(e)
                                       }}
                                       onFocus={(e)=>{e.target.value =this.state.EditObject.Address}}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <span> </span>
                                <input type="number" className="form-control"
                                       placeholder={this.state.EditObject.Coordinates[0].lat}
                                       onChange={(e) => {
                                           this.onEditLocationLat(e);
                                           this.isInputValid(e)
                                       }}
                                       onFocus={(e)=>{e.target.value = this.state.EditObject.Coordinates[0].lat}}
                                />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <span> </span>
                                <input type="number" className="form-control"
                                       placeholder={this.state.EditObject.Coordinates[1].lon}
                                       onChange={(e) => {
                                           this.onEditLocationLon(e);
                                           this.isInputValid(e)
                                       }}
                                       onFocus={(e)=>{e.target.value =this.state.EditObject.Coordinates[1].lon}}
                                />
                            </div>
                        </div>
                    </div>
                    <button className={this.state.EditFormValid ? "btn-success" : "buttonDisabled"}
                            disabled={!this.state.EditFormValid}
                            onClick={this.onSaveEditLocation}>                      save    </button>
                    <button className="btn-primary " onClick={this.canselEditLocation}>cansel</button>
                </div>
            </div>
        </div>);

    };


 //=========================================================================================================================


 //==================================New Location==========================================================================

    getAddLocationForm = () => {
        this.AddLocationRef["select"] = React.createRef();
        this.AddLocationRef["LocationName"] = React.createRef();
        this.AddLocationRef["Address"] = React.createRef();
        this.AddLocationRef["lat"] = React.createRef();
        this.AddLocationRef["lon"] = React.createRef();

        return (<div className={this.state.AddLocation ? "AddLocation modalOpen" : "AddLocation modalClose"}>
            <div className="AddLocationForm">
                <div className="form-group-sm AddLocationFormInputBox">
                    <div className="row">
                        <div className="col-md-6 ">
                            <select className="form-control" onChange={this.onSelectHandler}
                                    ref={this.AddLocationRef["select"]}>
                                <option default="ChooseCategoryName" value=''>ChooseCategoryName</option>
                                {this.getCategoryNameAsOption()}
                            </select>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <span> </span>
                                <input type="text" id="LocationName"
                                       disabled={!this.state.NamePicked.length > 0}
                                       placeholder="Type new Location Name"
                                       value={this.state.newCategoryName}
                                       ref={this.AddLocationRef["LocationName"]}
                                       onChange={(e) => {
                                           this.onInputLocationName(e);
                                           this.isInputValid(e)
                                       }}
                                       className="form-control AddCategoryFormInput"/>
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <span> </span>
                                <input type="Address" className="form-control" placeholder="Type Address"
                                       onChange={(e) => {
                                           this.onInputLocationAddress(e);
                                           this.isInputValid(e)
                                       }}
                                       ref={this.AddLocationRef["Address"]}
                                />
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <span> </span>
                                <input type="number" className="form-control" placeholder="Type lat"
                                       onChange={(e) => {
                                           this.onInputLocationLat(e);
                                           this.isInputValid(e)
                                       }}
                                       ref={this.AddLocationRef["lat"]}
                                />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <span> </span>
                                <input type="number" className="form-control " placeholder="Type lon"
                                       onChange={(e) => {
                                           this.onInputLocationLon(e);
                                           this.isInputValid(e)
                                       }}
                                       ref={ this.AddLocationRef["lon"]}
                                />
                            </div>
                        </div>
                    </div>
                    <button className={this.state.AddLocationFormValid ? "btn-success" : "buttonDisabled"}
                            disabled={!this.state.AddLocationFormValid}
                            onClick={this.onCreateNewLocation}
                    >save
                    </button>
                    <button className="btn-primary " onClick={this.canselNewLocation}>cansel</button>
                </div>
            </div>
        </div>);
    };

    addLocationHandler = () => {
        this.setState({AddLocation: true})
    };
    renewAddLocationForm = () => {

        this.AddLocationRef["select"].current.value = "";

        this.AddLocationRef["LocationName"].current.value = "";
        this.invalidateInput(this.AddLocationRef["LocationName"].current);

        this.AddLocationRef["Address"].current.value = "";
        this.invalidateInput(this.AddLocationRef["Address"].current);

        this.AddLocationRef["lat"].current.value = "";
        this.invalidateInput(this.AddLocationRef["lat"].current);

        this.AddLocationRef["lon"].current.value = "";
        this.invalidateInput(this.AddLocationRef["lon"].current);

    };
    canselNewLocation = () => {
        this.setState({AddLocation: false, NamePicked: "", AddLocationFormValid: false});
        this.AddLocation = {name: "", Address: "", Coordinates: [{lat: ""}, {lon: ""}], Category: "", selected: false};
        this.renewAddLocationForm();

    };
    onCreateNewLocation = () => {
        this.props.onAdd({...this.AddLocation});
        this.AddLocation = {name: "", Address: "", Coordinates: [{lat: ""}, {lon: ""}], Category: "", selected: false};
        this.setState({AddLocation: false, NamePicked: "", AddLocationFormValid: false});
        this.renewAddLocationForm();
    };

    onInputLocationName = (e) => {
        this.AddLocation.name = e.target.value;
        this.isFormValid(this.AddLocation);
    };
    onInputLocationAddress = (e) => {
        this.AddLocation.Address = e.target.value;
        this.isFormValid(this.AddLocation);
        if(this.isFormValid(this.AddLocation)){
            this.setState({AddLocationFormValid: true});
        }
    };
    onInputLocationLat = (e) => {
        this.AddLocation.Coordinates[0].lat = e.target.value;
        this.isFormValid(this.AddLocation);
        if(this.isFormValid(this.AddLocation)){
            this.setState({AddLocationFormValid: true});
        }
    };
    onInputLocationLon = (e) => {
        this.AddLocation.Coordinates[1].lon = e.target.value;
       if(this.isFormValid(this.AddLocation)){
           this.setState({AddLocationFormValid: true});
       }
    };
    onSelectHandler = (e) => {
        this.setState({NamePicked: e.target.value});
        this.AddLocation.Category = e.target.value;
        if(this.isFormValid(this.AddLocation)){
            this.setState({AddLocationFormValid: true});
        }
    };

//=====================================================================================================================
 //=============================Map====================================================================================
    onMapHandler =()=>{
     let selected =this.getSelected();
     if(selected.length ===1){
         let center ={lat:Number.parseFloat(selected[0].Coordinates[0].lat),
             lng:Number.parseFloat(selected[0].Coordinates[1].lon)};
         this.props.history.push({pathname:"/map",'center':center ,"name":selected[0].name});
     }else{
         alert("Choose attlist one or Only one Location")
     }
    };
//=================================Render==============================================================================
    render() {
let locations_ =this.getLocationsOfSelectedCat();
console.log(TAG+"locations ",locations_);
        return (
            <div >
                {this.getAddLocationForm()}
                {this.getEditLocation()}
                <nav className="navbar navbar-expand-sm  container headerBar">
                    <a className="navbar-brand" style={{fontSize: "22px"}}>MyLocations</a>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <button className="btn-primary" onClick={this.addLocationHandler}>AddLocation</button>
                        </li>
                        <li className="nav-item">
                            <button className="btn-primary" onClick={this.onDeleteLocationHandler}>Delete</button>
                        </li>
                        <li className="nav-item">
                            <button className="btn-primary" onClick={this.onEditHandler}>Edit</button>
                        </li>
                        <li className="nav-item">
                            <button className="btn-primary" onClick={this.onMapHandler}>Map</button>
                        </li>
                    </ul>
                </nav>
                <div className="Locations">
                    <div className="Content">
                        {locations_}
                    </div>
                    <Footer selectedButton="Locations"/>
                </div>
            </div>
        )
    }
}
//==============================Redux============================================================================
const mapStateToProps = state => {
    return {
        loc: state
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onDelete: (value) => dispatch(actionCreators.delete_l(value)),
        onEdit: (value) => dispatch(actionCreators.edit_l(value)),
        onAdd: (value) => dispatch(actionCreators.add_l(value)),
        onSelect: (value) => dispatch(actionCreators.select_l(value))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Locations));
