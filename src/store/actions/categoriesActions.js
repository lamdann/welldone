import * as actionTypes from './actionTypes';




export const delete_c = (value) => {
    return{
        type: actionTypes.DELETE_C,
        val: value
    };
};


export const save_c = (value ) => {
    return {
        type: actionTypes.SAVE_C,
        val: value
    };
};

export const edit_c = (value ) => {
    return {
        type: actionTypes.EDIT_C,
        val: value
    };
};

export const add_c = ( value ) => {
    return {
        type: actionTypes.ADD_C,
        val: value
    };
};

export const select_c = ( value ) => {
    return {
        type: actionTypes.SELECT_C,
        val: value
    };
};



