export const DELETE_C = 'DELETE_C';
export const DELETE_L = 'DELETE_L';
export const ADD_C = 'ADD_C';
export const ADD_L = 'ADD_L';
export const SAVE_L = 'SAVE_L';
export const SAVE_C = 'SAVE_C';
export const EDIT_L = 'EDIT_L';
export const EDIT_C = 'EDIT_C';
export const SELECT_C = 'SELECT_C';
export const SELECT_L = 'SELECT_L';

