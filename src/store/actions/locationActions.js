

import * as actionTypes from './actionTypes';

export const delete_l = (value) => {
    return{
        type: actionTypes.DELETE_L,
        val: value
    };
};
export const save_l = (value ) => {
    return {
        type: actionTypes.SAVE_L,
        val: value
    };
};

export const edit_l = (value ) => {
    return {
        type: actionTypes.EDIT_L,
        val: value
    };
};

export const add_l = ( value ) => {
    return {
        type: actionTypes.ADD_L,
        val: value
    };
};
export const select_l = ( value ) => {
    return {
        type: actionTypes.SELECT_L,
        val: value
    };
};

