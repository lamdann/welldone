import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';
import TAG from "../../myGat";

const SAVED_INITIAL_STATE ="SAVED_INITIAL_STATE";
let json= localStorage.getItem(SAVED_INITIAL_STATE);
if(!json) {
    json = `{
	"Categories":[{"name": "Israel","selected":false},{"name": "USA","selected":false},{"name": "Germany",	"selected":false}],
	"myLocations":[
		{"name":"Tel Aviv","Address": "Israel Tel Aviv-Yafo","Coordinates": [{"lat": "32.109333"},{"lon": "34.855499"}],"Category": "Israel","selected":false},
		{"name":"Ashdod","Address": "Israel Ashdod","Coordinates": [{"lat": "31.801447"},{"lon": "34.643497"}],"Category": "Israel","selected":false},		
		{"name":"New York City","Address": "New York City, NY, USA","Coordinates": [{"lat": "40.730610"},{"lon": "-73.935242"}],"Category": "USA","selected":false},
		{"name":"Hugo","Address":"Hugo, MN, USA","Coordinates": [{"lat": "45.159966"},{"lon": "-92.993340"}],"Category": "USA","selected":false},	
		{"name":"Berlin","Address": "Berlin,Germany","Coordinates": [{"lat": "52.520008"},{"lon": "13.404954"}],"Category": "Germany","selected":false}		
		]
	}`;
    localStorage.setItem(SAVED_INITIAL_STATE, json);

}

export const initialState =JSON.parse(json);
export const arrayCopy=(array)=> {
    let copyArray;
    copyArray = array.map((el)=>{
        if( Array.isArray(el)){
            return arrayCopy(el);
        }
        else return updateObject({},el);
    });
    return copyArray;
};

const reducer = ( state = initialState, action ) => {
    let Categories =null;
    let newState =null;
    let myLocations =null;
    switch ( action.type ) {

        case actionTypes.SELECT_C:
           Categories = arrayCopy(state.Categories);
           Categories = Categories.map((item)=>{
                if(item.name===action.val.name){ item.selected=!item.selected; }
                return item;
            });
         return updateObject(state,{Categories});

        case actionTypes.ADD_C:
         let newCategory = {name:action.val.name,selected:false};
         Categories = arrayCopy(state.Categories);
         Categories.push(newCategory);
         newState = updateObject(state, {Categories});
         localStorage.setItem(SAVED_INITIAL_STATE, JSON.stringify(newState));
        return newState;

        case actionTypes.DELETE_C:
            Categories = arrayCopy(state.Categories);
            myLocations = arrayCopy(state.myLocations);

            action.val.forEach((obj)=>{
            Categories  = Categories.filter((item)=>{ return item.name!==obj.name; });
            myLocations = myLocations.filter((item)=>{ return item.Category!==obj.name; }) ;
            });

            newState = updateObject(state, {Categories,myLocations});
            localStorage.setItem(SAVED_INITIAL_STATE, JSON.stringify(newState));
            return newState;


        case actionTypes.EDIT_C:
            Categories = arrayCopy(state.Categories);
            myLocations =arrayCopy(state.myLocations);

            Categories = Categories.map((item)=>{
                if(item.name === action.val.name){ item.name = action.val.newName; }
                return item;
            });
            myLocations = myLocations.map((item)=>{
                if(item.Category===action.val.name){ item.Category = action.val.newName;  }
                return item;
            });
            newState = updateObject(state, {Categories,myLocations});
            localStorage.setItem(SAVED_INITIAL_STATE, JSON.stringify(newState));

        return newState;

        case actionTypes.ADD_L:
            myLocations =[].concat(state.myLocations,action.val);
            newState = updateObject(state, {myLocations});
            localStorage.setItem(SAVED_INITIAL_STATE, JSON.stringify(newState));
            return newState;

        case actionTypes.EDIT_L:
            myLocations = arrayCopy(state.myLocations);
            Categories = arrayCopy(state.Categories);
            console.log(TAG+" action ",action.val);
            myLocations =myLocations.map((item)=>{
                if(item.Category===action.val.selected.Category){
                    if(JSON.stringify(item)===JSON.stringify(action.val.selected)){
                        item = updateObject(item,action.val.editedObject)
                    }
                }
                return item;
            });
            Categories = Categories.filter((categ)=>{
                return myLocations.find((loc)=>{
                    return categ.name===loc.Category;
                })
            });

            newState = updateObject(state, {Categories,myLocations});
            localStorage.setItem(SAVED_INITIAL_STATE, JSON.stringify(newState));

        return newState;
        case actionTypes.DELETE_L:
            myLocations = arrayCopy(state.myLocations);
            Categories = arrayCopy(state.Categories);
            action.val.forEach((obj)=>{
                myLocations = myLocations.filter((loc)=>{
                    if(obj.categoryName!==loc.Category){
                        return true;
                    }else{
                        return JSON.stringify(loc)!==obj.jsonValue;
                    }
                })
            });
            Categories = Categories.filter((categ)=>{
                    return myLocations.find((loc)=>{
                        return categ.name===loc.Category;
                    })
            });
            newState = updateObject(state, {Categories,myLocations});
            localStorage.setItem(SAVED_INITIAL_STATE, JSON.stringify(newState));

            return newState;

        case actionTypes.SELECT_L:
            myLocations = arrayCopy(state.myLocations);
            for(let item of myLocations ){
                if(item.Category===action.val.Category){
                    if(JSON.stringify(item)===action.val.item ){
                        item.selected = !item.selected
                    }
                }
            }
            return updateObject(state,{myLocations});
        default: return state;
    }
};

export default reducer;