import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import CategoryReducer from './store/reducers/categoriesReducer';

import { Provider } from 'react-redux';
import {BrowserRouter} from "react-router-dom";

import { createStore,  applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';


const logger = store => {
    return next => {
        return action => {
            console.log('[Middleware] Dispatching', action);
            const result = next(action);
            console.log('[Middleware] next state', store.getState());
            return result;
        }
    }
};
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(CategoryReducer,composeEnhancers(applyMiddleware(logger,thunk)));

ReactDOM.render(

        <Provider store={store}>
            <BrowserRouter>
               <App initialData={store.getState()}/>
           </BrowserRouter>
        </Provider>

    ,document.getElementById('root'));

registerServiceWorker();
