/**
 * Created by Lamdan on 3/28/2018.
 */
import * as actionTypes from '../src/store/actions/actionTypes';
import   {initialState} from '../src/store/reducers/categoriesReducer';
import {arrayCopy} from "../src/store/reducers/categoriesReducer";

import { updateObject } from '../src/store/utility';
const reducer = ( state = initialState, action ) => {
    let Categories =null;
    let newState =null;
    let myLocations =null;

    switch ( action.type ) {
        case actionTypes.ADD_L:
            return updateObject(state, {currentPage: action.val});
        case actionTypes.SAVE_L:
            return updateObject(state, {currentPage: action.val});
        case actionTypes.EDIT_L:
            return updateObject(state, {currentPage: action.val});
        case actionTypes.DELETE_L:
            return updateObject(state, {currentPage: action.val});

        case actionTypes.SELECT_L:
            myLocations = arrayCopy(state.myLocations);
            for(let item of myLocations ){
                if(item.Category===action.val.Category){
                    if(JSON.stringify(item)===action.val.item ){
                        item.selected = !item.selected
                    }
                }
            }
         return updateObject(state,{myLocations});
        default: return state;
    }

};

export default reducer;
